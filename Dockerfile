FROM python:3.9-slim as base

USER root

ENV TZ=Asia/Ho_Chi_Minh
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update -qq && \
    apt-get install -y --no-install-recommends \
    wget \
    python3 \
    #nginx \
    ipython3\
    build-essential\
    #python-dev\
    python3-dev\
    ca-certificates \
    python3-distutils \
    ffmpeg libsm6 libxext6 \
    tk poppler-utils \
    inkscape \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir -r /app/requirements.txt
RUN pip install aiohttp==3.8.3
RUN pip install patool wget
RUN pip install websockets==10.0
RUN pip install numpy==1.23.4
RUN pip install craft-text-detector==0.4.3
RUN python -m pip install paddlepaddle==2.5.1 -i https://pypi.tuna.tsinghua.edu.cn/simple
RUN pip install ultralytics
RUN wget http://nz2.archive.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.1f-1ubuntu2.23_amd64.deb
RUN dpkg -i libssl1.1_1.1.1f-1ubuntu2.23_amd64.deb
RUN pip install google-generativeai==0.7.2
RUN pip install json-repair
# RUN pip install decorator
# RUN pip install protobuf==3.20.0

COPY passport_reader /app/passport_reader
COPY models /app/models
COPY settings /app/settings

ENV PYTHONPATH "${PYTHONPATH}:/app/passport_reader"

WORKDIR /app
RUN chgrp -R 0 /app && chmod -R g=u /app
RUN mkdir /.cache /.paddleocr
RUN mkdir -p /.cache/torch/hub/checkpoints/
COPY models/anglecls/2020-11-16_resnext50_32x4d.zip /.cache/torch/hub/checkpoints/
RUN chgrp -R 0 /.cache /.paddleocr && chmod -R g=u /.cache /.paddleocr

USER 1001

CMD ["python", "./passport_reader/run.py"]
