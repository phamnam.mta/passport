from PIL import Image

def rotate_image_pil(image, angle):
    """
    Xoay ảnh sử dụng PIL, tương tự như np.rot90().
    
    :param image: PIL Image object
    :param angle: Số lần xoay 90 độ (1, 2, hoặc 3)
    :return: PIL Image object đã xoay
    """
    
    # Ánh xạ angle sang góc xoay tương ứng trong PIL
    rotation_map = {1: 90, 2: 180, 3: 270}
    
    # Xoay ảnh
    rotated_image = image.rotate(rotation_map[angle], expand=True)
    
    return rotated_image


# Mở ảnh
image = Image.open("/Users/namph/Downloads/9cea052d2d9289ccd083.jpg")

# Xoay ảnh
rotated_image = rotate_image_pil(image, 1)  # Xoay 90 độ ngược chiều kim đồng hồ

# Lưu ảnh đã xoay
rotated_image.save("rotated_image.jpg")