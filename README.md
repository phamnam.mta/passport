# Invoice Reader

## Install dependencies

```bash
pip install -r requirements.txt
```

## Development

### Add project root to Python Path

```bash
export PYTHONPATH=$PYTHONPATH:/Users/namph/Documents/passport-recognition/passport_reader
```

### Run service

```bash
python passport_reader/run.py
```


## Run Vietnamese OCR with Docker
### Build image
```bash
docker build --platform=linux/amd64 -t terado/passport_reader:0.1 .
```

### Save image as a tar file:
```bash
docker save -o <path for generated tar file> <image name>
```

### Load docker image from file:
```bash
docker load -i <path to image tar file>
```

### Run Vietnamese OCR service:
```bash
mkdir -p logs && touch logs/app.log && chmod 777 logs/app.log && docker run -d -e AUTH_TOKEN="GkpOK3YI629nLFUQ1NzbT2fz5lih16mQ" -p 10005:5005 -it terado/passport_reader:0.1
```

## How to integrate with Invoice Reader
### Call API with CURL
```bash
curl -X POST \
  http://localhost:5005/api/v1/parse_invoice \
  -F 'file=@xxx.xml'
```

### Response example
```json
```
=======
# passport


