from enum import Enum
class DocType(Enum):
    PDF = "pdf"
    IMAGE = "image"
    IB_BCTC = "IB_BCTC"
    HR_CTV = "HR_CTV"

class PDFType(Enum):
    SCANNED = "Scanned"
    DIGITALLY = "Digitally"
    SEARCHABLE = "Searchable"


PADDLE_DET_MODEL_DIR="models/en_PP-OCRv3_det_infer"
PADDLE_REC_MODEL_DIR="models/en_PP-OCRv3_rec_slim_infer"
PADDLE_CLS_MODEL_DIR="models/ch_ppocr_mobile_v2.0_cls_infer"

CRAFT_MODEL_PATH="models/craft/craft_mlt_25k.pth"
CRAFT_REFINER_MODEL_PATH="models/craft/craft_refiner_CTW1500.pth"


# server
DEFAULT_REQUEST_TIMEOUT = 60 * 5  # 5 minutes
DEFAULT_RESPONSE_TIMEOUT = 60 * 60  # 1 hour
DEFAULT_SERVER_PORT = 5005
DEFAULT_ENCODING = "utf-8"
TCP_PROTOCOL = "TCP"
DEFAULT_SANIC_WORKERS = 1
ENV_SANIC_WORKERS = "SANIC_WORKERS"
ENV_SANIC_BACKLOG = "SANIC_BACKLOG"
DEFAULT_LOG_LEVEL_LIBRARIES = "ERROR"
ENV_LOG_LEVEL_LIBRARIES = "LOG_LEVEL_LIBRARIES"
DEFAULT_SERVER_INTERFACE = "0.0.0.0"
