import cv2
import re
import os
import io
import numpy as np
import imghdr
from datetime import datetime
from PIL import Image, ImageEnhance
from passport_reader.utils.constants import PDFType


# def split_pages_pdf_to_files(data: bytes, temp_dir: str):
#     temp_paths = []
#     reader = PdfReader(io.BytesIO(data))
#     for i in range(len(reader.pages)):
#         output = PdfWriter()
#         output.addPage(reader.pages[i])
#         t = datetime.now()
#         time_str = t.strftime('%Y%m%d%H%M%S%f')
#         file_name = f"{time_str}_{i}.pdf"
#         pdf_path = os.path.join(temp_dir, file_name)
#         with open(pdf_path, "wb") as outputStream:
#             output.write(outputStream)
#             temp_paths.append(pdf_path)
#     return temp_paths

def calculate_w_h(points):
    a = points[0]
    b = points[1]
    c = points[2]
    d = points[3]

    # Calculate the height and width
    h = d[1] - a[1]
    w = c[0] - a[0]
    return w, h

def find_adjacent_elements(arr):
    max_value = float('-inf')
    min_distance = 0.05
    elements = ()

    for i in range(len(arr) - 1):
        distance = abs(arr[i+1] - arr[i])
        if distance < min_distance or (distance <= min_distance and arr[i+1] > max_value):
            max_value = max(arr[i], arr[i+1])
            elements = (arr[i], arr[i+1])

    return elements

def rectify_poly(img, poly):
    # Use Affine transform
    n = int(len(poly) / 2) - 1
    width = 0
    height = 0
    for k in range(n):
        box = np.float32([poly[k], poly[k + 1], poly[-k - 2], poly[-k - 1]])
        width += int(
            (np.linalg.norm(box[0] - box[1]) + np.linalg.norm(box[2] - box[3])) / 2
        )
        height += np.linalg.norm(box[1] - box[2])
    width = int(width)
    height = int(height / n)

    output_img = np.zeros((height, width, 3), dtype=np.uint8)
    width_step = 0
    for k in range(n):
        box = np.float32([poly[k], poly[k + 1], poly[-k - 2], poly[-k - 1]])
        w = int((np.linalg.norm(box[0] - box[1]) + np.linalg.norm(box[2] - box[3])) / 2)

        # Top triangle
        pts1 = box[:3]
        pts2 = np.float32(
            [[width_step, 0], [width_step + w - 1, 0], [width_step + w - 1, height - 1]]
        )
        M = cv2.getAffineTransform(pts1, pts2)
        warped_img = cv2.warpAffine(
            img, M, (width, height), borderMode=cv2.BORDER_REPLICATE
        )
        warped_mask = np.zeros((height, width, 3), dtype=np.uint8)
        warped_mask = cv2.fillConvexPoly(warped_mask, np.int32(pts2), (1, 1, 1))
        output_img[warped_mask == 1] = warped_img[warped_mask == 1]

        # Bottom triangle
        pts1 = np.vstack((box[0], box[2:]))
        pts2 = np.float32(
            [
                [width_step, 0],
                [width_step + w - 1, height - 1],
                [width_step, height - 1],
            ]
        )
        M = cv2.getAffineTransform(pts1, pts2)
        warped_img = cv2.warpAffine(
            img, M, (width, height), borderMode=cv2.BORDER_REPLICATE
        )
        warped_mask = np.zeros((height, width, 3), dtype=np.uint8)
        warped_mask = cv2.fillConvexPoly(warped_mask, np.int32(pts2), (1, 1, 1))
        cv2.line(
            warped_mask, (width_step, 0), (width_step + w - 1, height - 1), (0, 0, 0), 1
        )
        output_img[warped_mask == 1] = warped_img[warped_mask == 1]

        width_step += w
    return output_img

def get_image_ext(img_bytes):
    w = imghdr.what("", img_bytes)
    if w is None:
        w = "jpeg"
    return w

def load_img(img_bytes, gray: bool = False):
    alpha_channel = None
    nparr = np.frombuffer(img_bytes, np.uint8)
    if gray:
        np_img = cv2.imdecode(nparr, cv2.IMREAD_GRAYSCALE)
    else:
        np_img = cv2.imdecode(nparr, cv2.IMREAD_UNCHANGED)
        if len(np_img.shape) == 3 and np_img.shape[2] == 4:
            alpha_channel = np_img[:, :, -1]
            np_img = cv2.cvtColor(np_img, cv2.COLOR_BGRA2RGB)
        else:
            np_img = cv2.cvtColor(np_img, cv2.COLOR_BGR2RGB)

    return np_img, alpha_channel

def draw_ocr(image, boxes):
    for b in boxes:
        top_left = tuple(np.array(b[0][0], np.int32))
        bottom_right = tuple(np.array(b[0][2], np.int32))
        text = b[1][1]
        font = cv2.FONT_HERSHEY_PLAIN
        image = cv2.rectangle(image, top_left, bottom_right, (0,255,0), 1)
        image = cv2.putText(image, text, top_left, font, 0.5, (0,0,255), 1, cv2.LINE_AA)
    return image

def sorted_boxes(dt_boxes):
    """
    Sort text boxes in order from top to bottom, left to right
    args:
        dt_boxes(array):detected text boxes with shape [4, 2]
    return:
        sorted boxes(array) with shape [4, 2]
    """
    num_boxes = len(dt_boxes)
    sorted_boxes = sorted(dt_boxes, key=lambda x: (x[0][1], x[0][0]))
    _boxes = list(sorted_boxes)

    for i in range(num_boxes - 1):
        for j in range(i, 0, -1):
            if abs(_boxes[j + 1][0][1] - _boxes[j][0][1]) < 10 and \
                    (_boxes[j + 1][0][0] < _boxes[j][0][0]):
                tmp = _boxes[j]
                _boxes[j] = _boxes[j + 1]
                _boxes[j + 1] = tmp
            else:
                break
    return _boxes

def get_cropped(image, boxes):
    img = Image.fromarray(image)
    cropped_images = []
    boxes_valid = []
    for b in boxes:
        top_left = b[0]
        bottom_right = b[2]
        box = tuple(top_left + bottom_right)
        if box[2] > box[0] and box[3] > box[1]:
            cropped = img.crop(box)
            if cropped.size[1] != 0 and cropped.size[0] != 0:
                cropped_images.append(cropped)
                boxes_valid.append(b)
    return boxes_valid, cropped_images

def reject_outliers(data, m=3.):
   d = np.abs(data - np.median(data))
   mdev = np.median(d)
   s = d / (mdev if mdev else 1.)
   indexes = np.where(s >= m)
   return indexes[0].tolist()
    
def get_outliers(boxes, sentences):
    ratio_char_in_box = []
    for i, b in enumerate(boxes):
        [p0, p1, p2, p3] = b
        w = p1[0] - p0[0]
        h = p2[1] - p0[1]
        area = w*h
        if len(sentences[i]) == 0:
            ratio_char_in_box.append(-1)
            continue

        ratio_char = area/len(sentences[i])
        ratio_char_in_box.append(ratio_char)
    data = np.array(ratio_char_in_box)
    indexes = reject_outliers(data)

    for i, s in enumerate(sentences):
        if len(s) <= 1:
            indexes.append(i)
    
    return indexes

def clean_stamp(img_enhanced):
    img = np.array(img_enhanced)
    img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    # lower mask (0-10)
    lower_red = np.array([0,50,50])
    upper_red = np.array([10,255,255])
    mask0 = cv2.inRange(img_hsv, lower_red, upper_red)

    # upper mask (170-180)
    lower_red = np.array([170,50,50])
    upper_red = np.array([180,255,255])
    mask1 = cv2.inRange(img_hsv, lower_red, upper_red)

    # join my masks
    mask = mask0+mask1

    img[np.where(mask!=0)] = 255

    return img

def preprocess(img, factor: int=2.5):
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    image = Image.fromarray(img)
    enhancer = ImageEnhance.Sharpness(image).enhance(factor)
    if gray.std() < 30:
        enhancer = ImageEnhance.Contrast(enhancer).enhance(factor)
    
    new_image = clean_stamp(enhancer)
    return new_image

import fitz # pip install PyMuPDF

def page_type(page):
    page_area = abs(page.rect) # Total page area
    img_area = 0.0
    for block in page.get_text("RAWDICT")["blocks"]:
        if block["type"] == 1: # Type=1 are images
            bbox=block["bbox"]
            img_area += (bbox[2]-bbox[0])*(bbox[3]-bbox[1]) # width*height
    img_perc = img_area / page_area

    text_area = 0.0
    for b in page.get_text_blocks():
        r = fitz.Rect(b[:4])  # Rectangle where block text appears
        text_area = text_area + abs(r)
    text_perc = text_area / page_area

    if text_perc < 0.01: #No text = Scanned
        page_type = PDFType.SCANNED
    elif img_perc > .8:  #Has text but very large images = Searchable
        page_type = PDFType.SEARCHABLE
    else:
        page_type = PDFType.DIGITALLY
    return page_type