import os
from pathlib import Path

BASE_DIR = Path.cwd()
SETTINGS_DIR: Path = BASE_DIR.joinpath('settings')
SETTINGS_DIR.mkdir(parents=True, exist_ok=True)
LOGS_DIR: Path = BASE_DIR.joinpath('logs')
LOGS_DIR.mkdir(parents=True, exist_ok=True)

LOG_CONFIG_FILENAME="logging_config.yaml"