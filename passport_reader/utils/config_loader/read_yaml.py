from pathlib import Path

import yaml

from passport_reader.utils import settings
from passport_reader.utils.config_loader.config_interface import ConfigReaderInterface
from passport_reader.utils.config_loader.serializer import Struct


class YamlConfigReader(ConfigReaderInterface):

  def __init__(self):
    super(YamlConfigReader, self).__init__()

  def read_config_from_file(self, config_filename: str):
    conf_path = Path(__file__).joinpath(settings.SETTINGS_DIR, config_filename)
    with open(conf_path) as file:
      config = yaml.safe_load(file)
    config_object = Struct(**config)
    return config_object
