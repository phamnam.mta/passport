import os
import traceback
import tempfile
import aiohttp
import asyncio
import concurrent.futures
from pathlib import Path

from functools import reduce, wraps
from http import HTTPStatus
from typing import (
    Any,
    Callable,
    List,
    Optional,
    Text,
    Union,
    Dict,
    Coroutine
)
from inspect import isawaitable
from sanic import Sanic, response
from sanic.request import Request
from sanic.response import HTTPResponse
from sanic_cors import CORS
from sanic_jwt import Initialize, exceptions

from passport_reader.processor import Processor
from passport_reader.utils.constants import (
    DEFAULT_RESPONSE_TIMEOUT,
    DocType
)

from passport_reader.utils.logger.custom_logging import logger_instance

logger = logger_instance.get_logger(__name__)


class ErrorResponse(Exception):
    """Common exception to handle failing API requests."""

    def __init__(
        self,
        status: Union[int, HTTPStatus],
        reason: Text,
        message: Text,
        details: Any = None,
        help_url: Optional[Text] = None,
    ) -> None:
        """Creates error.
        Args:
            status: The HTTP status code to return.
            reason: Short summary of the error.
            message: Detailed explanation of the error.
            details: Additional details which describe the error. Must be serializable.
            help_url: URL where users can get further help (e.g. docs).
        """
        self.error_info = {
            "status": "failure",
            "message": message,
            "reason": reason,
            "details": details or {},
            "help": help_url,
            "code": status,
        }
        self.status = status
        logger.error(message)
        super(ErrorResponse, self).__init__()


def ensure_loaded_agent(
    app: Sanic, require_core_is_ready: bool = False
) -> Callable[[Callable], Callable[..., Any]]:
    """Wraps a request handler ensuring there is a loaded and usable agent.
    Require the agent to have a loaded Core model if `require_core_is_ready` is
    `True`.
    """

    def decorator(f: Callable) -> Callable:
        @wraps(f)
        def decorated(*args: Any, **kwargs: Any) -> Any:
            # noinspection PyUnresolvedReferences
            if not app.agent or not app.agent.is_ready():
                raise ErrorResponse(
                    HTTPStatus.CONFLICT,
                    "Conflict",
                    "No agent loaded. To continue processing, a "
                    "model of a trained agent needs to be loaded.",
                )

            return f(*args, **kwargs)

        return decorated

    return decorator

def requires_auth(app: Sanic, token: Optional[Text] = None) -> Callable[[Any], Any]:
    """Wraps a request handler with token authentication."""

    def decorator(f: Callable[[Any, Any], Any]) -> Callable[[Any, Any], Any]:

        @wraps(f)
        async def decorated(request: Request, *args: Any, **kwargs: Any) -> Any:

            provided = request.headers.get("token", None)

            # noinspection PyProtectedMember
            if token is not None and provided == token:
                result = f(request, *args, **kwargs)
                if isawaitable(result):
                    result = await result
                return result
            elif token is None and app.config.get("USE_JWT") is None:
                # authentication is disabled
                result = f(request, *args, **kwargs)
                if isawaitable(result):
                    result = await result
                return result
            raise ErrorResponse(
                HTTPStatus.UNAUTHORIZED,
                "NotAuthenticated",
                "User is not authenticated."
            )

        return decorated

    return decorator

def validate_request_files(request: Request, error_message: Text):
    """Check if `request` has a file."""
    try:
        file = request.files['file'][0]
        return file
    except Exception as e:
        logger.debug(traceback.format_exc())
        raise ErrorResponse(HTTPStatus.BAD_REQUEST,
                            "BadRequest", error_message)
    
def validate_request_id_cards(request: Request, error_message: Text):
    """Check if `request` has a file."""
    try:
        id_front = request.files['id_front'][0]
        id_back = request.files['id_back'][0]
        return id_front, id_back
    except Exception as e:
        logger.debug(traceback.format_exc())
        raise ErrorResponse(HTTPStatus.BAD_REQUEST,
                            "BadRequest", error_message)

def create_ssl_context(
    ssl_certificate: Optional[Text],
    ssl_keyfile: Optional[Text],
    ssl_ca_file: Optional[Text] = None,
    ssl_password: Optional[Text] = None,
) -> Optional["SSLContext"]:
    """Create an SSL context if a proper certificate is passed.
    Args:
        ssl_certificate: path to the SSL client certificate
        ssl_keyfile: path to the SSL key file
        ssl_ca_file: path to the SSL CA file for verification (optional)
        ssl_password: SSL private key password (optional)
    Returns:
        SSL context if a valid certificate chain can be loaded, `None` otherwise.
    """

    if ssl_certificate:
        import ssl

        ssl_context = ssl.create_default_context(
            purpose=ssl.Purpose.CLIENT_AUTH, cafile=ssl_ca_file
        )
        ssl_context.load_cert_chain(
            ssl_certificate, keyfile=ssl_keyfile, password=ssl_password
        )
        return ssl_context
    else:
        return None

async def authenticate(request: Request):
    """Callback for authentication failed."""
    raise exceptions.AuthenticationFailed(
        "Direct JWT authentication not supported. You should already have "
        "a valid JWT from an authentication provider, Rasa will just make "
        "sure that the token is valid, but not issue new tokens."
    )


def configure_cors(
    app: Sanic, cors_origins: Union[Text, List[Text], None] = ""
) -> None:
    """Configure CORS origins for the given app."""

    # Workaround so that socketio works with requests from other origins.
    # https://github.com/miguelgrinberg/python-socketio/issues/205#issuecomment-493769183
    app.config.CORS_AUTOMATIC_OPTIONS = True
    app.config.CORS_SUPPORTS_CREDENTIALS = True

    CORS(
        app, resources={r"/*": {"origins": cors_origins or ""}}, automatic_options=True
    )

def async_if_callback_url(f: Callable[..., Coroutine]) -> Callable:
    """Decorator to enable async request handling.
    If the incoming HTTP request specified a `callback_url` query parameter, the request
    will return immediately with a 204 while the actual request response will
    be sent to the `callback_url`. If an error happens, the error payload will also
    be sent to the `callback_url`.
    Args:
        f: The request handler function which should be decorated.
    Returns:
        The decorated function.
    """

    @wraps(f)
    async def decorated_function(
        request: Request, *args: Any, **kwargs: Any
    ) -> HTTPResponse:
        callback_url = request.form.get("callback_url")
        # Only process request asynchronously if the user specified a `callback_url`
        # query parameter.
        if not callback_url:
            return await f(request, *args, **kwargs)

        async def wrapped() -> None:
            try:
                result: HTTPResponse = await f(request, *args, **kwargs)
                payload: Dict[Text, Any] = dict(
                    data=result.body, headers={"Content-Type": result.content_type}
                )
                logger.debug(
                    "Asynchronous processing of request was successful. "
                    "Sending result to callback URL."
                )

            except Exception as e:
                if not isinstance(e, ErrorResponse):
                    logger.error(e)
                    e = ErrorResponse(
                        HTTPStatus.INTERNAL_SERVER_ERROR,
                        "UnexpectedError",
                        f"An unexpected error occurred. Error: {e}",
                    )
                # If an error happens, we send the error payload to the `callback_url`
                
                user_id = request.form.get("id")
                if user_id:
                    result = response.json({
                        "id": user_id,
                        "data": e.error_info,
                        "status": e.error_info.get("code", 500)
                    })
                    payload: Dict[Text, Any] = dict(
                    data=result.body, headers={"Content-Type": result.content_type}
                )
                else:
                    payload = dict(json=e.error_info)
                
                # payload = dict(
                #     data=e.error_info, headers={"Content-Type": result.content_type}
                # )
                logger.error(
                    "Error happened when processing request asynchronously. "
                    "Sending error to callback URL."
                )
            async with aiohttp.ClientSession() as session:
                await session.post(callback_url, raise_for_status=True, **payload)

        # Run the request in the background on the event loop
        request.app.add_task(wrapped())

        # The incoming request will return immediately with a 204
        return response.empty()

    return decorated_function


def run_in_thread(f: Callable[..., Coroutine]) -> Callable:
    """Decorator which runs request on a separate thread.
    Some requests (e.g. training or cross-validation) are computional intense requests.
    This means that they will block the event loop and hence the processing of other
    requests. This decorator can be used to process these requests on a separate thread
    to avoid blocking the processing of incoming requests.
    Args:
        f: The request handler function which should be decorated.
    Returns:
        The decorated function.
    """

    @wraps(f)
    async def decorated_function(
        request: Request, *args: Any, **kwargs: Any
    ) -> HTTPResponse:
        # Use a sync wrapper for our `async` function as `run_in_executor` only supports
        # sync functions
        def run() -> HTTPResponse:
            return asyncio.run(f(request, *args, **kwargs))

        with concurrent.futures.ThreadPoolExecutor() as pool:
            return await request.app.loop.run_in_executor(pool, run)

    return decorated_function


def inject_temp_dir(f: Callable[..., Coroutine]) -> Callable:
    """Decorator to inject a temporary directory before a request and clean up after.
    Args:
        f: The request handler function which should be decorated.
    Returns:
        The decorated function.
    """

    @wraps(f)
    async def decorated_function(*args: Any, **kwargs: Any) -> HTTPResponse:
        with tempfile.TemporaryDirectory() as directory:
            # Decorated request handles need to have a parameter `temporary_directory`
            return await f(*args, temporary_directory=Path(directory), **kwargs)

    return decorated_function


def create_app(
    agent: Optional["Processor"] = None,
    cors_origins: Union[Text, List[Text], None] = "*",
    auth_token: Optional[Text] = None,
    response_timeout: int = DEFAULT_RESPONSE_TIMEOUT,
    jwt_secret: Optional[Text] = None,
    jwt_method: Text = "HS256",
):
    """Class representing a Rasa HTTP server."""

    app = Sanic(__name__)
    app.config.RESPONSE_TIMEOUT = response_timeout
    configure_cors(app, cors_origins)

    # Setup the Sanic-JWT extension
    if jwt_secret and jwt_method:
        # since we only want to check signatures, we don't actually care
        # about the JWT method and set the passed secret as either symmetric
        # or asymmetric key. jwt lib will choose the right one based on method
        app.config["USE_JWT"] = True
        Initialize(
            app,
            secret=jwt_secret,
            authenticate=authenticate,
            algorithm=jwt_method,
            user_id="username",
        )

    app.agent = agent

    @app.exception(ErrorResponse)
    async def handle_error_response(request: Request, exception: ErrorResponse):
        return response.json(exception.error_info, status=exception.status)

    @app.get("api/v1/status")
    @ensure_loaded_agent(app)
    def status(request: Request):
        """Determine if the container is working and healthy. In this sample container, we declare
        it healthy if we can load the model successfully."""

        return response.text('OCR server is up and running.')

    @app.post('api/v1/id_card')
    @requires_auth(app, auth_token)
    @ensure_loaded_agent(app)
    @async_if_callback_url
    @run_in_thread
    async def extract_id_card(request: Request):

        id_front, id_back = validate_request_id_cards(
            request,
            "The request is missing the required `file`."
        )
        try:
            name = id_front.name
            id_front_bytes = id_front.body
            id_back_bytes = id_back.body
            ext = name.split(".")[-1]
            
            if ext not in ["png", "jpeg", "jpg"]:
                raise ErrorResponse(
                    400,
                    "Bad request",
                    f"{ext} format is not supported. Required format is png, jpg or jpeg",
                )

            response_data = app.agent.extract_id_card([id_front_bytes, id_back_bytes])
            
            return response.json(response_data)
        except Exception as e:
            print(traceback.format_exc())
            raise ErrorResponse(
                500,
                "Error",
                f"An unexpected error occurred during report extraction. Error: {e}",
            )


    @app.post('api/v1/text')
    @requires_auth(app, auth_token)
    @ensure_loaded_agent(app)
    @async_if_callback_url
    @run_in_thread
    async def extract_mrz(request: Request):

        data = validate_request_files(
            request,
            "The request is missing the required `file`."
        )
        try:
            name = data.name
            origin_bytes = data.body
            ext = name.split(".")[-1]
            
            if ext not in ["png", "jpg", "jpeg", "jfif", "pjpeg", "pjp"]:
                raise ErrorResponse(
                    400,
                    "Bad request",
                    f"{ext} format is not supported. Required format is pdf, png, jpg or jpeg",
                )

            response_data = app.agent.extract_mrz(origin_bytes, cropped=False)
            
            return response.json(response_data)
        except Exception as e:
            print(traceback.format_exc())
            raise ErrorResponse(
                500,
                "Error",
                f"An unexpected error occurred during report extraction. Error: {e}",
            )
        
    @app.post('api/v1/text-by-crop')
    @requires_auth(app, auth_token)
    @ensure_loaded_agent(app)
    @async_if_callback_url
    @run_in_thread
    async def extract_mrz(request: Request):

        data = validate_request_files(
            request,
            "The request is missing the required `file`."
        )
        try:
            name = data.name
            origin_bytes = data.body
            ext = name.split(".")[-1]
            
            if ext not in ["png", "jpg", "jpeg", "jfif", "pjpeg", "pjp"]:
                raise ErrorResponse(
                    400,
                    "Bad request",
                    f"{ext} format is not supported. Required format is pdf, png, jpg or jpeg",
                )

            response_data = app.agent.extract_mrz(origin_bytes, cropped=True)
            
            return response.json(response_data)
        except Exception as e:
            print(traceback.format_exc())
            raise ErrorResponse(
                500,
                "Error",
                f"An unexpected error occurred during report extraction. Error: {e}",
            )

    @app.post('api/v1/angle')
    @requires_auth(app, auth_token)
    @ensure_loaded_agent(app)
    @async_if_callback_url
    @run_in_thread
    async def predict_angle(request: Request):

        data = validate_request_files(
            request,
            "The request is missing the required `file`."
        )
        try:
            name = data.name
            origin_bytes = data.body
            ext = name.split(".")[-1]
            
            if ext not in ["png", "jpg", "jpeg", "jfif", "pjpeg", "pjp"]:
                raise ErrorResponse(
                    400,
                    "Bad request",
                    f"{ext} format is not supported. Required format is pdf, png, jpg or jpeg",
                )

            response_data = app.agent.predict_angle(origin_bytes)
            
            return response.json(response_data)
        except Exception as e:
            print(traceback.format_exc())
            raise ErrorResponse(
                500,
                "Error",
                f"An unexpected error occurred during report extraction. Error: {e}",
            )

    return app
