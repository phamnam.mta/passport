import numpy as np
from typing import Union, Text
import os
import copy

from craft_text_detector import Craft
from passport_reader.utils.constants import (
    CRAFT_MODEL_PATH,
    CRAFT_REFINER_MODEL_PATH,
)
from passport_reader.utils.helper import (
    calculate_w_h,
    find_adjacent_elements,
    rectify_poly,
)


class MRZDetector:
    def __init__(self) -> None:
        self.model = Craft(crop_type="box", link_threshold=0.2, cuda=False, weight_path_craft_net=CRAFT_MODEL_PATH, weight_path_refine_net=CRAFT_REFINER_MODEL_PATH)

    def process(self, image: Union[Text, np.ndarray]):
        prediction_result = self.model.detect_text(copy.deepcopy(image))
        boxes = sorted(prediction_result["boxes"].tolist(), key=lambda x: (x[0][1], x[0][0]))
        boxes_as_ratios = sorted(prediction_result["boxes_as_ratios"].tolist(), key=lambda x: (x[0][1], x[0][0]))

        arr_wid = []
        candidates = []
        for idx, p in enumerate(boxes_as_ratios):
            w, h = calculate_w_h(p)
            n = max([w, h])
            if n > 0.4:
                arr_wid.append(max([w, h]))
                candidates.append(boxes[idx])
        result = find_adjacent_elements(arr_wid)
        if result:
            box1 = candidates[arr_wid.index(result[0])]
            box2 = candidates[arr_wid.index(result[1])]

            crop1 = rectify_poly(image, box1)
            crop2 = rectify_poly(image, box2)

            return {
                "lines": [crop1, crop2],
                "boxes": [box1, box2]
            }
        else:
            return None