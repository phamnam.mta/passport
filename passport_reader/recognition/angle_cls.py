import numpy as np
from typing import Union, Text
import cv2
import torch
import albumentations as albu
from check_orientation.pre_trained_models import create_model


class AngleCLS:
    def __init__(self) -> None:
        self.model = create_model("swsl_resnext50_32x4d")
        self.model.eval()
        self.transform = albu.Compose([albu.Resize(height=224, width=224), albu.Normalize(p=1)], p=1)
        self.mapping = {
            1: 3,
            2: 2,
            3: 1
        }

    def process(self, image: Union[Text, np.ndarray]):
        x = self.transform(image=image)["image"]

        image = np.ascontiguousarray(np.transpose(x, (2, 0, 1)))
        temp = [torch.from_numpy(image)]

        with torch.no_grad():
            prediction = self.model(torch.stack(temp)).numpy()
        
        scores = [round(tx, 2) for tx in prediction[0]]
        max_score = max(scores)
        idx = scores.index(max_score)

        rot = 0
        if max_score >= 0.85 and idx != 0:
            rot = self.mapping[idx]
        return rot