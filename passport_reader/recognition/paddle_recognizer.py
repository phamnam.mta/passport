import numpy as np
import copy
from typing import Union, Text

from paddleocr import PaddleOCR
from passport_reader.utils.constants import (
    PADDLE_DET_MODEL_DIR,
    PADDLE_REC_MODEL_DIR,
    PADDLE_CLS_MODEL_DIR
)
from passport_reader.utils.helper import (
    calculate_w_h,
    find_adjacent_elements,
    rectify_poly,
)


class PaddleRecognizer:
    def __init__(self) -> None:
        self.recognizer = PaddleOCR(det_model_dir=PADDLE_DET_MODEL_DIR,
                                    rec_model_dir=PADDLE_REC_MODEL_DIR,
                                    cls_model_dir=PADDLE_CLS_MODEL_DIR,
                                    use_angle_cls=True, 
                                    use_space_char=False,
                                    lang='en')
        
    def detect_mrz(self, image: Union[Text, np.ndarray]):
        prediction_result = self.recognizer.ocr(image, rec=False, cls=False)
        if not prediction_result:
            return None
        
        boxes = sorted(prediction_result[0], key=lambda x: (x[0][1], x[0][0]))
        # get image size
        img_height = image.shape[0]
        img_width = image.shape[1]

        # calculate box coords as ratios to image size
        boxes_as_ratios = []
        for box in boxes:
            boxes_as_ratios.append((np.array(box) / [img_width, img_height]).tolist())

        arr_wid = []
        candidates = []
        for idx, p in enumerate(boxes_as_ratios):
            w, h = calculate_w_h(p)
            n = max([w, h])
            if n > 0.4:
                arr_wid.append(max([w, h]))
                candidates.append(boxes[idx])
        result = find_adjacent_elements(arr_wid)
        if result:
            box1 = candidates[arr_wid.index(result[0])]
            box2 = candidates[arr_wid.index(result[1])]

            crop1 = rectify_poly(image, box1)
            crop2 = rectify_poly(image, box2)

            return {
                "lines": [crop1, crop2],
                "boxes": [box1, box2]
            }
        else:
            return None

    def process(self, image: Union[Text, np.ndarray]):
        result = self.recognizer.ocr(image, det=False, cls=False)
        if result:
            text = result[0][0][0]
            score = result[0][0][1]
            return text, score
        else:
            return '', 0
