import os
import google.generativeai as genai
from PIL import Image
import numpy as np
from io import BytesIO
import json
from json_repair import repair_json
from enum import Enum
import re
import typing_extensions as typing
from typing import Literal

class DataValid(Enum):
    VALID = 0
    PARTIALLY_VALID = 1
    INVALID = 2

  

class CitizenCard(typing.TypedDict):
    id_number: str
    full_name: str
    date_of_birth: str
    gender: str
    nationality: str
    place_of_origin: str
    place_of_residence: str
    date_of_issue: str
    date_of_expiry: str
    place_of_origin_is_in_vietnam: bool
    mrz: str
    

SAFETY_SETTINGS = [
  {
    "category": "HARM_CATEGORY_HARASSMENT",
    "threshold": "BLOCK_NONE"
  },
  {
    "category": "HARM_CATEGORY_HATE_SPEECH",
    "threshold": "BLOCK_NONE"
  },
  {
    "category": "HARM_CATEGORY_SEXUALLY_EXPLICIT",
    "threshold": "BLOCK_NONE"
  },
  {
    "category": "HARM_CATEGORY_DANGEROUS_CONTENT",
    "threshold": "BLOCK_NONE"
  },
]

# Create the model
generation_config = {
  "temperature": 0,
  # "top_p": 0.95,
  # "top_k": 64,
  "max_output_tokens": 500,
  "response_mime_type": "application/json",
  # "response_schema": CitizenCard
}

model = genai.GenerativeModel(
  model_name="gemini-2.0-flash",
  safety_settings=SAFETY_SETTINGS,
  generation_config=generation_config,
  # safety_settings = Adjust safety settings
  # See https://ai.google.dev/gemini-api/docs/safety-settings
)

model2 = genai.GenerativeModel(
  model_name="gemini-2.0-flash",
  safety_settings=SAFETY_SETTINGS,
  generation_config=generation_config,
  # safety_settings = Adjust safety settings
  # See https://ai.google.dev/gemini-api/docs/safety-settings
)

PROMPT = """Bạn là một AI OCR chuyên trích xuất thông tin từ hình ảnh căn cước công dân Việt Nam (cả mặt trước và mặt sau) và trả về dữ liệu theo định dạng JSON.

**Đầu vào**: Hai hình ảnh: mặt trước và mặt sau của một căn cước công dân Việt Nam.

**Đầu ra**: Một JSON object chứa các thông tin được trích xuất từ hai hình ảnh theo schema sau:

class CitizenID(TypedDict):
    id_number: str  # Số cước công dân, lấy ở phần `Số / No.` hoặc ở phần `Số định danh cá nhân`
    full_name: str  # Họ và tên đầy đủ ghi trên thẻ căn cước.
    date_of_birth: str  # Ngày tháng năm sinh theo định dạng dd/mm/yyyy
    gender: Literal['Nam', 'Nữ']  # Giới tính
    nationality: str  # Lấy ở phần `Quốc tịch / Nationality`
    place_of_origin: str  # Lấy ở phần `Quê quán / Place of Origin` hoặc `Nơi đăng ký khai sinh / Place of birth` trên ảnh. Lấy đầy đủ thông tin đơn vị hành chính từ cấp tỉnh trở xuống.
    place_of_residence: str  # Lấy ở phần `Nơi thường trú / Place of residence` hoặc `Nơi cư trú / Place of residence` trên ảnh. Lấy đầy đủ thông tin địa chỉ thường trú/cư trú có trong ảnh.
    date_of_issue: str  # Ngày, tháng, năm cấp căn cước công dân, lấy ở phần phía trên con dấu và chữ ký. Lấy ngày ở phần phía trên con dấu và chữ ký, cạnh phần đọc chip, ngay sau phần `Ngày, tháng, năm / Date, month, year:` ở trên ảnh mặt sau.
    date_of_expiry: str  # Ngày, tháng, năm hết hạn theo định dạng dd/mm/yyyy, lấy ở phần `Date of expiry`
    place_of_origin_is_in_vietnam: bool  # Xác định place_of_origin có thuộc Việt Nam hay không. True nếu thuộc Việt Nam, False nếu không.
    mrz: str  # Dữ liệu MRZ (Machine Readable Zone) trên mặt sau của căn cước, với số ký tự nhỏ hơn 100.

**Lưu ý**:

* id_number: Số cước công dân, lấy ở phần `Số / No.` hoặc ở phần `Số định danh cá nhân`
* date_of_issue: Là ngày cấp căn cước, tránh nhầm lẫn với ngày hết hạn căn cước. Lấy theo định dạng dd/mm/yyyy.
* place_of_residence: Lấy ở phần `Nơi thường trú / Place of residence` hoặc `Nơi cư trú / Place of residence` trên ảnh. Lấy đầy đủ thông tin địa chỉ thường trú/cư trú có trong ảnh.
* place_of_origin: "Lấy ở phần `Quê quán / Place of Origin` hoặc `Nơi đăng ký khai sinh / Place of birth` trên ảnh. Lấy đầy đủ thông tin đơn vị hành chính từ cấp tỉnh trở xuống."
* Các trường `date_of_birth`, `date_of_expiry` và `date_of_issue` phải tuân thủ định dạng `dd/mm/yyyy`.
* Trường `mrz` chỉ chứa tối đa 100 ký tự.
"""

def validate(json_data) -> DataValid:
    if any(i not in json_data for i in ["full_name", "gender", "date_of_birth", "id_number", "date_of_expiry", "date_of_issue", "place_of_residence", "place_of_origin", "mrz"]):
        return DataValid.INVALID
    elif str(json_data.get("id_number", "")) not in str(json_data.get("mrz", "")):
        return DataValid.INVALID
    elif not bool(re.fullmatch(r'\d{12}', json_data["id_number"])):
        return DataValid.PARTIALLY_VALID
    else:
        return DataValid.VALID
    
def post_process(response):
  has_text = True
  try:
    response.text
  except:
    has_text = False
  if response.prompt_feedback.block_reason.value == 0 and has_text:
    print("response", response)
    response_message = {}
    try:
      response_message = json.loads(response.text)
    except:
      response_message = repair_json(response.text)
    
    if response_message and isinstance(response_message, list):
      response_message = response_message[0]
    
    data_valid = validate(response_message)
    response_message["data_valid"] = data_valid.name
    if "place_of_origin" in response_message and "place_of_residence" in response_message:
      if response_message["place_of_origin_is_in_vietnam"]:
        response_message["place_of_origin"] = response_message["place_of_origin"].rsplit(",", 1)[-1].strip()
      else:
        response_message["place_of_origin"] = response_message["place_of_residence"].rsplit(",", 1)[-1].strip()
  else:
      response_message = None
  return response_message
    

def extract(image1: Image, image2: Image):
    genai.configure(api_key="AIzaSyD5Dy1pVW4dIvYx4YYBVoYkG6gzHixRG2U")
    try:
      response = model.generate_content([image1, image2, PROMPT],
                                        generation_config=generation_config, safety_settings=SAFETY_SETTINGS,
                                        request_options={"timeout": 120})
      response_message = post_process(response)
    except Exception as e:
       print(e)
       response_message = None

    if not response_message:
      response = model2.generate_content([image1, image2, PROMPT],
                                      generation_config=generation_config, safety_settings=SAFETY_SETTINGS,
                                      request_options={"timeout": 120})
        
      response_message = post_process(response)
    
    return response_message
