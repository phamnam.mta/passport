import asyncio
import logging
import uuid
import os
from functools import partial
from typing import Any, List, Optional, Text, Union, Dict


from sanic import Sanic

from passport_reader import server
from passport_reader.processor import Processor
from passport_reader.utils import sanic_utils
from passport_reader.utils.constants import (
    DEFAULT_RESPONSE_TIMEOUT,
    DEFAULT_SERVER_PORT,
    DEFAULT_SERVER_INTERFACE,
    ENV_SANIC_BACKLOG
)
from passport_reader.utils.logger.custom_logging import logger_instance

logger = logger_instance.get_logger(__name__)


def configure_app(
    cors: Optional[Union[Text, List[Text], None]] = None,
    auth_token: Optional[Text] = None,
    response_timeout: int = DEFAULT_RESPONSE_TIMEOUT,
    jwt_secret: Optional[Text] = None,
    jwt_method: Optional[Text] = None,
    log_file: Optional[Text] = None,
) -> Sanic:
    """Run the agent."""
    # sanic_utils.configure_file_logging(logger, log_file)

    app = server.create_app(
        cors_origins=cors,
        auth_token=auth_token,
        response_timeout=response_timeout,
        jwt_secret=jwt_secret,
        jwt_method=jwt_method,
    )

    # if logger.isEnabledFor(logging.DEBUG):
    #     sanic_utils.list_routes(app)

    # async def configure_async_logging() -> None:
    #     if logger.isEnabledFor(logging.DEBUG):
    #         sanic_utils.enable_async_loop_debugging(asyncio.get_event_loop())

    # app.add_task(configure_async_logging)

    return app


def serve_application(
    interface: Optional[Text] = DEFAULT_SERVER_INTERFACE,
    port: int = DEFAULT_SERVER_PORT,
    cors: Optional[Union[Text, List[Text]]] = "*",
    auth_token: Optional[Text] = None,
    response_timeout: int = DEFAULT_RESPONSE_TIMEOUT,
    jwt_secret: Optional[Text] = None,
    jwt_method: Optional[Text] = None,
    log_file: Optional[Text] = None,
    ssl_certificate: Optional[Text] = None,
    ssl_keyfile: Optional[Text] = None,
    ssl_ca_file: Optional[Text] = None,
    ssl_password: Optional[Text] = None,
    use_syslog: Optional[bool] = False,
    syslog_address: Optional[Text] = None,
    syslog_port: Optional[int] = None,
    syslog_protocol: Optional[Text] = None,
) -> None:
    """Run the API entrypoint."""

    app = configure_app(
        cors,
        auth_token,
        response_timeout,
        jwt_secret,
        jwt_method,
    )

    ssl_context = server.create_ssl_context(
        ssl_certificate, ssl_keyfile, ssl_ca_file, ssl_password
    )
    protocol = "https" if ssl_context else "http"

    logger.debug(
        f"Starting OCR server on {protocol}://{interface}:{port}")

    app.register_listener(
        partial(load_agent_on_start),
        "before_server_start",
    )
    # app.register_listener(close_resources, "after_server_stop")

    number_of_workers = sanic_utils.number_of_sanic_workers()

    sanic_utils.update_sanic_log_level(
        log_file, use_syslog, syslog_address, syslog_port, syslog_protocol,
    )

    app.run(
        host=interface,
        port=port,
        ssl=ssl_context,
        backlog=int(os.environ.get(ENV_SANIC_BACKLOG, "100")),
        workers=number_of_workers,
    )


def load_agent_on_start(
    app: Sanic,
    loop: Text,
) -> Processor:
    """Load an agent.
    Used to be scheduled on server start
    (hence the `app` and `loop` arguments)."""
    app.agent = Processor()
    return app.agent


if __name__ == '__main__':
    token = os.getenv("AUTH_TOKEN", None)
    serve_application(auth_token=token)
