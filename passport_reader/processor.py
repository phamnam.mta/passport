import io
import time
from PIL import Image
import numpy as np
from typing import List, Union, Text, Dict
from datetime import datetime
from io import BytesIO

from passport_reader.utils.helper import load_img

from passport_reader.recognition.paddle_recognizer import PaddleRecognizer
from passport_reader.recognition.angle_cls import AngleCLS
# from passport_reader.recognition.mrz_detector import MRZDetector
from passport_reader.recognition.gemini import extract

from passport_reader.utils.logger.custom_logging import logger_instance
logger = logger_instance.get_logger(__name__)


class Processor:
    def __init__(self) -> None:
        self.rec = PaddleRecognizer()
        self.angle_cls = AngleCLS()
        # self.mrz = MRZDetector()

    def is_ready(self) -> bool:
        """Check if all necessary components are instantiated to use processor."""
        return self.rec is not None and self.angle_cls is not None
    
    def rotate_image_pil(self, image, angle):
        """
        Xoay ảnh sử dụng PIL, tương tự như np.rot90().
        
        :param image: PIL Image object
        :param angle: Số lần xoay 90 độ (1, 2, hoặc 3)
        :return: PIL Image object đã xoay
        """
        
        # Ánh xạ angle sang góc xoay tương ứng trong PIL
        rotation_map = {1: 90, 2: 180, 3: 270}
        
        # Xoay ảnh
        rotated_image = image.rotate(rotation_map[angle], expand=True)
        
        return rotated_image
    
    def extract_id_card(self, data: List[bytes]) -> Dict:
        image1 = Image.open(BytesIO(data[0]))
        image2 = Image.open(BytesIO(data[1]))

        start = time.time()
        temp, _ = load_img(data[0])
        angle = self.angle_cls.process(temp)
        if angle > 0:
            image1 = self.rotate_image_pil(image1, angle)

        temp, _ = load_img(data[1])
        angle = self.angle_cls.process(temp)
        if angle > 0:
            image2 = self.rotate_image_pil(image2, angle)
        logger.debug(f"angle_predict time: {(time.time() - start) * 1000}ms")
        
        res = extract(image1, image2)
        return res

    def extract_mrz(self, data: bytes, cropped=False) -> List:
        image, _ = load_img(data)
        mrz_data = None
        if not cropped:
            start = time.time()
            angle = self.angle_cls.process(image)
            if angle > 0:
                image = np.rot90(image, angle)
            logger.debug(f"angle_predict time: {(time.time() - start) * 1000}ms")

            start = time.time()
            mrz_data = self.rec.detect_mrz(image)
            logger.debug(f"Mrz detect time: {(time.time() - start) * 1000}ms")
        else:
            image = Image.open(io.BytesIO(data))
            width, height = image.size

            split_image1 = image.crop((0, 0, width, height/2)).convert('RGB') 
            split_image2 = image.crop((0, height/2, width, height)).convert('RGB') 

            mrz_data = {
                "lines": [np.array(split_image1), np.array(split_image2)],
                "boxes": [[[0, 0], [width, 0], [width, height/2], [0, height/2]], [[0, height/2], [width, height/2], [width, height], [0, height]]]
            }

        start = time.time()
        text1, score1 = self.rec.process(mrz_data["lines"][0])
        text2, score2 = self.rec.process(mrz_data["lines"][1])
        logger.debug(f"Ocr time: {(time.time() - start) * 1000}ms")

        text1 = "".join(e for e in text1 if e.isalnum() or e == "<").upper()
        text2 = "".join(e for e in text2 if e.isalnum() or e == "<").upper()
        
        return {
            "mrz_text": [text1, text2],
            "boxes": mrz_data["boxes"],
            "scores": [score1, score2]
        }

    def predict_angle(self, data: bytes) -> List:
        image, _ = load_img(data)

        start = time.time()
        angle = self.angle_cls.process(image)
        logger.debug(f"angle_predict time: {(time.time() - start) * 1000}ms")
        
        return {
            "angle": angle,
        }

